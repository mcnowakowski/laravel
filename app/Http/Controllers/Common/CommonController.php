<?php

namespace App\Http\Controllers\Common;

// use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CommonController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCurrentRoutePath(Request $request)
    {
    	return Route::getFacadeRoot()->current()->uri();
        // return Route::getCurrentRoute()->getPath();
    	// return Route::currentRouteName();
    	// return Route::getCurrentRoute()->getActionName();
    	// return Request::capture()->fullUrl();
		// return Request::capture()->getUri();
		// return Request::capture()->getRequestUri();
    }

}
