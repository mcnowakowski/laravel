<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Entities\Country;

use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::orderBy('id')->get();

        return response()->json([
            'countries' => $countries
        ]);
    }
}
