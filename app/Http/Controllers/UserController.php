<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
    	$users = User::all();

    	return response()->json([
    		'users' => $users
    	]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email')
        ]);
        return response()->json([
            'message' => 'user created successfully',
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $user = User::find($id);
        $user->update($request->all());
        return response()->json([
            'message' => 'user updated successfully'
        ]);
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return response()->json([
            'message' => 'User deleted successfully'
        ]);
    }
}
