import VueRouter from 'vue-router';

var widgets = [];

let routes = [
	{
		path: '/',
		component: require('../../../Modules/Dashboard/Resources/assets/js/components/Widgets')
	},
	{
		path: '/dashboard',
		component: require('../../../Modules/Dashboard/Resources/assets/js/components/Widgets')
	},
	{
		path: '/users',
		component: require('./components/Users')
	},
	{
		path: '/auction-system',
		component: require('../../../Modules/Auction/Resources/assets/js/components/AuctionSystem')
	}
];

export default new VueRouter({
	routes,
	linkActiveClass: 'is-active'
});