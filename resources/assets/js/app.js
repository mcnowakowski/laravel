
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import bootstrap from './bootstrap';
import router from './routes';

// User
Vue.component('user', require('./components/User.vue'));
Vue.component('home', require('./components/Home.vue'));
Vue.component('auction-system-small', require('./components/User.vue'));

// Dashboard
Vue.component('sidebar', require("../../../Modules/Dashboard/Resources/assets/js/components/Sidebar.vue"));

// Widget
Vue.component('widget-loader', require("../../../Modules/Dashboard/Resources/assets/js/components/WidgetLoader.vue"));
// Vue.component('widgets', require("../../../Modules/Dashboard/Resources/assets/js/components/Widgets.vue"));

// Auction
Vue.component('auction-system', require("../../../Modules/Auction/Resources/assets/js/components/AuctionSystem.vue"));

new Vue({
	el: '#app',
	data: {
		baseurl: 'http://system.app/',
		widgets: [],
		sidebars: []
	},
	router,
	methods: {
        clearWidgets: function() {
	    	this.widgets = [];
	    }
    }
});
