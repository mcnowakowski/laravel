export default {
  baseUrl = 'http://system.app'
}


// in some component.vue
import config from './config';

export default {
  methods: {
    someMethod: function () {
      if (config.baseUrl) {
        ....
      }
    }
  }
}