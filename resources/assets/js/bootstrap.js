
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');

// require('bootstrap-sass');
require('quasar-framework/dist/quasar.mat.css');

// window.Vue = require('vue');
// Vue.use(require('vue-router'));
// window.axios = require('axios');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import axios from 'axios';
import Quasar from 'quasar-framework';
// import Vuetify from 'vuetify';

window.Vue = Vue;
Vue.use(Quasar);
// Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueResource);
window.axios = axios;

window.axios.defaults.headers.common = {
	'X-CSRF-TOKEN': window.Laravel.csrfToken,
	'X-Requested-With': 'XMLHttpRequest'
};