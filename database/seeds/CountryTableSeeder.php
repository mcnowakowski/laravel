<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::connection('mysql_common')->table('country')->delete();
        DB::connection('mysql_common')->table('country')->insert([
        	[
	        	'id' => 1,
	            'title' => 'Poland'
	        ],[
	        	'id' => 2,
	            'title' => 'England'
	        ]
	    ]);
    }
}
