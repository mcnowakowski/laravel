<?php

namespace Modules\Dashboard\Entities;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $table = 'widget';

    public $timestamps = false;

    protected $fillable = [
    	'title','icon','route', 'box_class', 'box_size', 'more_info', 'type',
    ];

    public function sidebarWidget()
	{
		return $this->hasMany('Modules\Dashboard\Entities\SidebarWidget');
	}
}
