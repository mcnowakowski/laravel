<?php

namespace Modules\Dashboard\Entities;

use Modules\Dashboard\Entities\Sidebar;
use Illuminate\Database\Eloquent\Model;

class SidebarWidget extends Model
{
	protected $table = 'sidebar_widget';

    public $timestamps = false;

    protected $fillable = [
    	'sidebar_id','widget_id','order',
    ];

    public function sidebar()
	{
		return $this->belongsTo('Modules\Dashboard\Entities\Sidebar', 'sidebar_id');
	}

	public function widget()
	{
		return $this->belongsTo('Modules\Dashboard\Entities\Widget', 'widget_id');
	}
}
