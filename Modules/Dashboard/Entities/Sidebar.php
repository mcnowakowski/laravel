<?php

namespace Modules\Dashboard\Entities;

use Modules\Dashboard\Entities\SidebarWidget;
use Illuminate\Database\Eloquent\Model;

class Sidebar extends Model
{
	protected $table = 'sidebar';

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','parent_id','icon','route','order',
    ];

    public function sidebarWidget()
	{
		return $this->hasMany('Modules\Dashboard\Entities\SidebarWidget');
	}
}
