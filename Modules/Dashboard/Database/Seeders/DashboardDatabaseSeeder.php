<?php

namespace Modules\Dashboard\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DashboardDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(SidebarTableSeeder::class);
    	$this->call(WidgetTableSeeder::class);
    	$this->call(SidebarWidgetTableSeeder::class);
    }
}