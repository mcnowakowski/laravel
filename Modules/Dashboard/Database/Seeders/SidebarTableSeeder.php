<?php

namespace Modules\Dashboard\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SidebarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sidebar')->delete();
        DB::table('sidebar')->insert([
            [
                'id' => 1,
                'title' => 'Dashboard',
                'parent_id' => 0,
                'icon' => 'dashboard',
                'route' => 'dashboard',
                'order' => 0,
                'default' => true
            ]
        ]);
    }
}
