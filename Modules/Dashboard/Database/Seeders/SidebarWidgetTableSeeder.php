<?php

namespace Modules\Dashboard\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SidebarWidgetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sidebar_widget')->delete();
        DB::table('sidebar_widget')->insert([
            [
                'sidebar_id' => 1,
                'widget_id' => 1,
                'order' => 0
            ]
        ]);
    }
}
