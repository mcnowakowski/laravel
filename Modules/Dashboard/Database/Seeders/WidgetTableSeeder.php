<?php

namespace Modules\Dashboard\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class WidgetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('widget')->delete();
        DB::table('widget')->insert([
            [
                'id' => 1,
                'title' => 'Auction Systems',
                'icon' => 'fa fa-sitemap',
                'route' => 'auction-system',
                'box_class' => 'box small-box text-white bg-primary',
                'box_size' => 'width-1of4',
                'more_info' => true,
                'type' => 'info-box'
            ]
        ]);
    }
}
