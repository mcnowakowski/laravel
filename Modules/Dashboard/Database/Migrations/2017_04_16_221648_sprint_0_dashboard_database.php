<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sprint0DashboardDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sidebar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->integer('parent_id')->unsigned()->index()->default(0);
            $table->string('icon');
            $table->string('route')->index();
            $table->integer('order')->unsigned()->index();
            $table->boolean('default')->default(false);
            $table->unique(['id', 'order'], 'sidebar_id_order_index');
            $table->unique(['parent_id', 'order'], 'sidebar_parent_id_order_index');
        });
        // maybe later
        // Schema::create('sidebar_notification', function (Blueprint $table) {
        //     $table->string('sidebar_id')->index();
        //     $table->string('notification_id')->index();
        // });
        Schema::create('widget', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->string('icon');
            $table->string('route');
            $table->string('box_class');
            $table->string('box_size')->default('col-lg-3 col-xs-6');
            $table->boolean('more_info')->default(false);
            $table->enum('type', ['info-box', 'chart', 'small-box', 'box']);
        });
        Schema::create('sidebar_widget', function (Blueprint $table) {
            $table->integer('sidebar_id')->unsigned()->index();
            $table->integer('widget_id')->unsigned()->index();
            $table->integer('order')->index();
            $table->unique(['sidebar_id', 'order'], 'sidebar_widget_sidebar_id_order_index');
        });
        Schema::table('sidebar_widget', function(Blueprint $table) {
            $table->foreign('sidebar_id')->references('id')->on('sidebar')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('sidebar_widget', function(Blueprint $table) {
            $table->foreign('widget_id')->references('id')->on('widget')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sidebar_widget');
        Schema::dropIfExists('widget');
        Schema::dropIfExists('sidebar');
    }
}
