<?php

Route::group(['middleware' => 'web', 'prefix' => 'dashboard', 'namespace' => 'Modules\Dashboard\Http\Controllers'], function()
{
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('sidebar', 'SidebarController');
    Route::get('/sidebar/{id}/getWidgetsBySidebarId/', 'SidebarController@getWidgetsBySidebarId');
    Route::get('sidebar.getWidgetsByDefaultSidebar', 'SidebarController@getWidgetsByDefaultSidebar');
    Route::resource('widget', 'WidgetController');
    // Route::get('/widget/{id}/getSidebarWidgetByWidgetId/', 'WidgetController@getSidebarWidgetByWidgetId');
});

// Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//     echo'<pre>';
//     var_dump($query->sql);
//     var_dump($query->bindings);
//     var_dump($query->time);
//     echo'</pre>';
// });

