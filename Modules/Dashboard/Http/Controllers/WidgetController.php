<?php

namespace Modules\Dashboard\Http\Controllers;

use Modules\Dashboard\Entities\Widget;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class WidgetController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $widgets = Widget::orderBy('id')->get();
        return response()->json([
            'widgets' => $widgets
        ]);
    }

    public function getSidebarWidgetByWidgetId($id)
    {
        $widgets = Widget::find($id);
        $sidebarWidgetArray = array();

        foreach ($widgets->sidebarWidget as $sidebarWidget) {
            $sidebarWidgetArray[] = $sidebarWidget;
        }

        return response()->json([
            'sidebarWidget' => $sidebarWidgetArray
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dashboard::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('dashboard::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('dashboard::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
