<?php

namespace Modules\Dashboard\Http\Controllers;

use Modules\Dashboard\Entities\Sidebar;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class SidebarController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $sidebars = Sidebar::orderBy('order')->get();
        return response()->json([
            'sidebars' => $sidebars
        ]);
    }

    public function getWidgetsBySidebarId($id)
    {
        $sidebars = Sidebar::find($id);
        $widgets = array();

        foreach ($sidebars->sidebarWidget as $sidebarWidget) {
            $widgets[] = $sidebarWidget->widget;
        }

        return response()->json([
            'widgets' => $widgets
        ]);
    }

    public function getWidgetsByDefaultSidebar()
    {
        $sidebars = Sidebar::where('default', 1)->first();
        $widgets = array();

        foreach ($sidebars->sidebarWidget as $sidebarWidget) {
            $widgets[] = $sidebarWidget->widget;
        }

        return response()->json([
            'widgets' => $widgets
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return Sidebar::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
