@extends('dashboard::layouts.master')

@section('content')
	<q-layout id="app" class="wrapper">
	 	@include('dashboard::layouts.header')
	 	@include('dashboard::layouts.sidebar')
	 	<!-- IF USING subRoutes only: -->
		<router-view class="layout-view" v-bind:widgets="widgets"></router-view>
		<!-- OR ELSE, IF NOT USING subRoutes: -->
		<!-- <div class="layout-view"></div> -->

	 	<!-- <div class="content-wrapper">
	 		<section class="content-header">
	 			<h1>Dashboard</h1>
	 			<ol class="breadcrumb">
	 			  <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	 			</ol>
	 		</section>
	 		<section class="content row">
	 			<div v-for="(item, index) in widget">
	 				<widget-box></widget-box>
	 			</div>
	 		</section>
	 	</div> -->
	 	@include('dashboard::layouts.footer')
	</q-layout>
@stop
