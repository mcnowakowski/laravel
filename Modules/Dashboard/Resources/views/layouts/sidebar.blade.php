<input type="hidden" value="{{ url('/') }}" v-model="baseurl" />
<sidebar
  v-on:clear-widgets="clearWidgets"
  v-bind:sidebars="sidebars"
  v-bind:baseurl="baseurl"
  v-bind:widgets="widgets">
</sidebar>


