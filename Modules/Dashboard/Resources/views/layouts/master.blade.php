<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>{{ config('app.name', 'AuctionSystem') }}</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<!-- <link href="{{ asset("/third_party/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" /> -->
		<!-- vuetifyjs -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet" type="text/css">
		<!-- Font Awesome -->
		<link href="{{ asset("css/font-awesome.css") }}" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style
		<!-- <link href="{{ asset("third_party/AdminLTE/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" /> -->
		<!-- <link href="{{ asset("third_party/AdminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" /> -->
		<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.1/css/bulma.min.css" rel="stylesheet"> -->

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">

		<!-- Vuetify Styles -->
    	<!-- <link href="{{ asset('css/vuetify.min.css') }}" rel="stylesheet"> -->

		<!-- Scripts -->
		<script>
		    //get the token from the meta tag
		    window.Laravel = {!! json_encode([
		        'csrfToken' => csrf_token(),
		    ]) !!};
		</script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
        @yield('content')

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Vuetify JS SCRIPTS -->
    <!-- <script src="{{ asset('js/vuetify.min.js') }}"></script> -->

	 <!-- jQuery 2.1.3 -->
	<!-- <script src="{{ asset ("third_party/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script> -->

	<!-- Bootstrap 3.3.7 JS -->
	<script src="{{ asset ("third_party/AdminLTE/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>

	<!-- AdminLTE App -->
	<script src="{{ asset ("third_party/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>

	<!-- Optionally, you can add Slimscroll and FastClick plugins.
	     Both of these plugins are recommended to enhance the
	     user experience. Slimscroll is required when using the
	     fixed layout. -->
	</body>
</html>