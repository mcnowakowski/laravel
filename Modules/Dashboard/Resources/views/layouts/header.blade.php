<div slot="header" class="toolbar tertiary">
    <button
      class="hide-on-drawer-visible"
      @click="$refs.drawer.open()"
    >
    	<i>menu</i>
    </button>
    <q-toolbar-title>
    	<router-link to="/">SMS</router-link>
    </q-toolbar-title>
    <ul class="breadcrumb">
	  	<li>
		  	<router-link to="/">
		      	<i>dashboard</i>
	  			<q-tooltip class="tertiary" anchor="center left" self="center right">
	  				dashboard
				</q-tooltip>
		    </router-link>
	  	</li>
	</ul>
    <button>
    	<a href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
			<i>exit_to_app</i>
			<q-tooltip anchor="center left" self="center right">
				<div class="item-link" @click="logout(), $refs.popover.close()">
                	logout
	      		</div>
		  	</q-tooltip>
		  	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	            {{ csrf_field() }}
	        </form>
	    </a>
    </button>
</div>