<?php

namespace Modules\Auction\Http\Controllers;

use Modules\Auction\Entities\AuctionSystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AuctionSystemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // $auctionSystem = AuctionSystem::orderBy('id')->get();
        // return response()->json([
        //     'auctionSystem' => $auctionSystem
        // ]);
        return view('auction::index');
    }

    public function getTotalAuctionSystem()
    {
        $total = AuctionSystem::sum('id');
        return response()->json([
            'totalAuctionSystem' => $total
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
