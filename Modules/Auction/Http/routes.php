<?php

Route::group(['middleware' => 'web', 'prefix' => 'auction', 'namespace' => 'Modules\Auction\Http\Controllers'], function()
{
    Route::get('/auction-system', 'AuctionSystemController@index');
    Route::get('auction-system.getTotalAuctionSystem', 'AuctionSystemController@getTotalAuctionSystem');
});
