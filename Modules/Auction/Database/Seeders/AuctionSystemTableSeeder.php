<?php

namespace Modules\Auction\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AuctionSystemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('mysql_auction')->table('auction_system')->delete();
        DB::connection('mysql_auction')->table('auction_system')->insert([
            [
                'id' => 1,
                'title' => 'Ebay',
                'created_at' => date("Y-m-d H:i:s")
            ]
        ]);
    }
}
