@extends('dashboard::layouts.master')

@section('content')
	<v-app id="app" class="wrapper" top-toolbar left-fixed-sidebar sidebar-under-toolbar>
	 	@include('dashboard::layouts.header')
	 	<main>
		 	@include('dashboard::layouts.sidebar')
        	<div class="content-wrapper">
		 		<section class="content-header">
		 			<h4>Auction</h4>
		 			<ol class="breadcrumb">
		 			  <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		 			</ol>
		 		</section>
		 		<section class="row">
		 			<v-content>
	        			<v-container fluid>
				 			<transition name="fade">
								<keep-alive>
				 					<router-view :widgets="widgets"></router-view>
				 				</keep-alive>
							</transition>
						</v-container>
    				</v-content>
		 		</section>
		 	</div>
	 	</main>
	 	@include('dashboard::layouts.footer')
	</v-app>
@stop
