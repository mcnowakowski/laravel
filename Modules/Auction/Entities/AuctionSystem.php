<?php

namespace Modules\Auction\Entities;

use Illuminate\Database\Eloquent\Model;

class AuctionSystem extends Model
{
    protected $connection = 'mysql_auction';

    protected $table = 'auction_system';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'deleted_at',
    ];

        /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
