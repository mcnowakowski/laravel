<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div id="app" class="panel-body">
                    <!-- <router-link tag="li" to="/">
                        <a>Home</a>
                    </router-link>
                    <router-link tag="li" to="/users">
                        <a>Users</a>
                    </router-link>
                    <router-view></router-view> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>