<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Redirect::guest('login');
// });

// Route::get('/home', function () {
Route::get('/', function () {
    return view('home');
});

Route::resource('user','UserController');

//Common
Route::get('country','Common\CountryController@index');
Route::get('getCurrentRoutePath','Common\CommonController@getCurrentRoutePath');

Auth::routes();
