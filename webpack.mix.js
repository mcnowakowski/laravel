const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js');
mix.sass('resources/assets/sass/app.scss', 'public/css');
// mix.js('resources/assets/js/vue-resource.min.js', 'public/js');
// mix.js('resources/assets/js/vue.min.js', 'public/js');
mix.sass('bower_components/font-awesome/css/font-awesome.css', 'public/css');

// mix.js('resources/assets/js/app.js', 'public/js');
// mix.sass('resources/assets/bulma.sass', 'public/css');
// mix.sass('resources/assets/sass/app.scss', 'public/css');

// mix.combine([
//   'resources/assets/mixer/css/bootstrap.min.css',
// ], 'public/dist/app.css');

// if (mix.config.inProduction) {
//   mix.version();
// } else {
//   mix.version();
// }
